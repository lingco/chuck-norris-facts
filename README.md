# Chuck Norris Facts

Welcome to 'Chuck Norris Facts' - the cutting-edge web-app designed to deliver all your Chuck Norris trivia. It's built using ReactJS and the free [ChuckNorris.io](https://api.chucknorris.io/) API.

It's pretty simple - just click the image of Chuck Norris to load a new fact!

## Requirements

To work on this project you'll need to have NodeJS version 12 or greater installed. You can use the code editor of your choice.

## Getting Started with Development

- Clone the project using your `git` client of choice
- Run `npm install` to install all dependencies
- Run `npm start` to start the Webpack Dev Server

This should build the project, open up a new tab in your browser and load the app in it.

At this point you can open the project in your code editor and start making code changes. After you've updated a source file the Webpack Dev Server should automatically reload the changes in your browser.

## Assignments

The app isn't quite usable in it's current state. You'll need to finish the app by fixing a few bugs and adding a few features. Check out the reference image in the section below to see (roughly) how it should look once you're done.

### 1: Minor Design Tweaks

1. The "Chuck Norris" image isn't loading. Figure out what's going on and fix that.
1. Update the footer so the copyright text stays on the right side of the page.
1. Put a thin red line between the footer and the content above it.

### 2: Fetch Facts

We'll leverage a free REST API for this from [ChuckNorris.io](https://api.chucknorris.io/):

`https://api.chucknorris.io/jokes/random?category=dev`

Use the excellent ["axios"](https://github.com/axios/axios) Javascript library to help you do this.

TheChuck component is currently just an image of Chuck. Change the app so that when you click on TheChuck it retrieves a new fact from the API and displays it in the fact box.

**Note:** the API call itself should not be done in TheChuck component - that should be done in MainContent. MainContent just needs to know when "TheChuck" was clicked so it can use axios to load the new Chuck fact.

### 3: Create the FactContainer component

Look at the MainContent component. You'll notice that it has a `div` element with a class named "fact-container". Move that `div` (and it's children) into a new component, named FactContainer. Make sure the current 'fact' gets passed from MainContent into the new component.

### 4: Add the "Fact Count"

If you look at the reference image below you'll see a fact count beneath the current fact. Update the app to add this functionality. Every time you load a new fact that number should be incremented and updated on the screen.

### 5: Hide the help text

"Click the Chuck to load a fact!" should only show before you've loaded a fact. It doesn't make any sense to see this instruction after that - update the app to fix this.

## Bonus

If you get through the assignments quickly you can always impress us with these bonus features.

### 1: Show a 'busy' indicator

Right now when the app is loaded a new fact, the user has no visual indication that anything is happening. Fix this, using any technique you'd like - it can be a simple text message, a cool animation, etc.

### 2: Persist data to browser storage

If you refresh the page the app state resets - it doesn't retain the current fact or the number of facts shown.

Change the app to persist both of these to the brower's local storage, and to load them from storage when the app first starts. This way you see both pieces of data immediately when the page is refreshed.

## Reference Screenshot

When it's done it should look roughly like this:

![Reference Image](/docs/chuck-norris-facts-reference.png)
