import React, { useState } from 'react';
import TheChuck from './TheChuck';

function MainContent() {
  const [fact, setFact] = useState('Facts should go here');

  return (
    <main className='app-content'>
      <TheChuck />
      <div className='fact-container'>
        <div className='help-text'>Click the Chuck to load a fact!</div>
        <div className='fact'>{fact}</div>;
      </div>
    </main>
  );
}
export default MainContent;
