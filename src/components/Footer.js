import React from 'react';

function Footer() {
  const note = `2020`;
  return <footer>&copy; {note}</footer>;
}
export default Footer;
