import React from 'react';

function TheChuck(props) {
  return (
    <img src='/the-chuck.jpg' alt='Chuck Norris' className='the-chuck'></img>
  );
}
export default TheChuck;
