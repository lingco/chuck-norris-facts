import React from 'react';

function Header() {
  const greeting = 'Indisputable Chuck Norris Facts';
  return <header>{greeting}</header>;
}
export default Header;
